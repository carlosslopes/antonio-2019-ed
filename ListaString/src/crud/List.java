package crud;

public class List implements IList {

	List(int tamanho) {
		String[] list = new String[tamanho];
		for (int i = 0; i < list.length; i++) {
			list[i] = "";
		}
		vet = list;
	}

	String vet[];

	@Override
	public void create(String novaPalavra) {
		for (int i = 0; i < vet.length; i++) {
			if (vet[i].equals("")) {
				vet[i] += novaPalavra;
				return;
			}
		}
		
		String[] vet2 = new String[vet.length*2];
		for (int i = 0; i < vet.length; i++) {
			vet2[i] = vet[i];
		}
		if(vet2[vet.length].equals(novaPalavra)) {
			vet = vet2;
		}
	}

	@Override
	public Integer read(String pegaPalavra) {
		for (int i = 0; i < vet.length; i++) {
			if(vet[i].equals(pegaPalavra)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public void update(String antes, String depois) {
		for (int i = 0; i < vet.length; i++) {
			if(vet[i].equals(antes)) {
				vet[i] = depois;
				return;
			} 
		}
		System.out.println("n�o existe essa palavra no vetor. ");
		return;
	}

	@Override
	public void delete(String deletePalavra) {
		Integer posicao;
		posicao = read(deletePalavra);
		if(posicao == -1) {
			System.out.println("\n\nn�o existe essa palavra no vetor. " + "\n"); 
			return;
		}
		String aux;
		vet[posicao] = null;
		for (int i = posicao; i < vet.length-1; i++) {
			aux = vet[i];
			vet[i] = vet[i+1];
			vet[i+1] = aux;
		}

	}

	@Override
	public void imprime() {
		for (int i = 0; i < vet.length; i++) {
			if(vet[i] != null) {
				System.out.print("|" + vet[i] + "| ");
			}

		}

	}

}

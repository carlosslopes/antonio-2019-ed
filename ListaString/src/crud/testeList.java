package crud;

public class testeList {

	public static void main(String[] args) {

		executa(new List(5));
	}

	private static void executa(List list) {
		System.out.println("lista vazia: ");
		list.imprime();
		list.create("A");
		list.create("B");
		list.create("C");
		list.create("D");
		list.create("E");
		System.out.println("\nlista preenchida: ");
		list.imprime();
		list.delete("A");
		System.out.println("\ndeletada: ");
		list.imprime();
		list.update("B", "A");
		System.out.println("\nupdate: ");
		list.imprime();
		System.out.println("\nLer indice: ");
		System.out.println(list.read("A"));
		
	}

}

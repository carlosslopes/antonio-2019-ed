package crud;

public interface IList {

	
	void create(String novaPalavra);
	
	Integer read(String pegaPalavra);
	
	void update(String antes, String depois);
	
	void delete(String deletePalavra);
	
	void imprime();
}

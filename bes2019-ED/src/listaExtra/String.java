
package listaExtra;

import java.util.Scanner;

public class String implements IString {

	char[] valores;

	public String() {
		Scanner entrada = new Scanner(System.in);
		valores = entrada.nextLine().toCharArray();
	}

	public String(char a) {
	}

	public int length() {
		return valores.length;
	}

	public char charAt(int posicao) {
		return valores[posicao];
	}

	public boolean equals(String valor) {
		int i = 0;
		System.out.println(valores[i]);
		while (true) {
			if (valor.length() == valores.length || valor.charAt(i) != valores[i]) {
				return false;
			} else if (i == valor.length() || i == valores.length) {
				return true;
			}
			i++;
		}
	}

	public boolean startsWith(String valor) {
		if (valor.charAt(0) == valores[0]) {
			return true;
		} else
			return false;
	}

	public boolean endWith(String valor) {
		if (valor.valores.length > valores.length) {
			return false;
		} else {
			for (int i = 0, j = (valores.length - valor.valores.length); i < valor.valores.length; i++) {
				if (valor.charAt(i) != valores[j]) {
					return false;
				}
			}
			return true;

		}
	}

	public int indexOf(char letra) {
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] == letra) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndexOf(char letra) {
		for (int i = valores.length; i > 0; i--) {
			if (valores[i] == letra) {
				return i;
			}
		}
		return -1;
	}

	public String substring(int inicio, int quantidadeDeCaracteres) {
		String novo = new String('a');
		novo.valores = new char[quantidadeDeCaracteres];
		for (int i = inicio, j = 0; i < (quantidadeDeCaracteres + inicio); i++, j++) {
			novo.valores[j] = valores[i];
		}
		return novo;
	}

	@Override
	public String replace(char letraASerTrocada, char letraATrocar) {
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] == letraASerTrocada) {
				valores[i] = letraATrocar;
			}
		}
		return this;
	}

	public String concat(String valor) {
		String novo = new String('b');
		novo.valores = new char[(valores.length + valor.valores.length)];
		for (int i = 0; i < valores.length; i++) {
			novo.valores[i] = valores[i];
		}
		for (int i = valores.length, j = 0; i < valor.valores.length; i++, j++) {
			novo.valores[i] = valor.valores[j];
		}
		return novo;
	}

	public void imprime() {
		for (char l : valores) {
			System.out.print(l);
		}
	}

}

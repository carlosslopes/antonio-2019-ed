package myList;

public class listaSE implements ILSE {

	No inicio = null;

	@Override
	public void inserir(int valor) {

		No novo = new No();
		novo.valor = valor;

		if (inicio == null)
			inicio = novo;
		else {
			No aux = inicio;
			No ant = null;
			while (aux.prox != null && valor > aux.valor) {
				aux = aux.prox;
				ant = aux;
			}
			aux.prox = novo;
			if(valor < aux.valor) {
				ant = ant.prox;
			}else if(valor < inicio.valor) {
				
			}
		}

	}

	@Override
	public void remover(int chave) {
		if(inicio == null) {
			System.out.println("Lista Vazia! ");
		}else {
			No aux = inicio;
			No ant = null;
			while(aux.prox != null && aux.valor != chave) {
				ant = aux;
				aux = aux.prox;
			}
			if(aux.valor == chave) {
				if(ant == null) {
					inicio = inicio.prox;
					aux = null;
				}else if(aux.prox == null) {
					ant.prox = null;
					aux = null;
				}else {
					ant.prox = aux.prox;
					aux = null;
				}
				
			}else
				System.out.println("Elemento n�o encontrado! ");
		}
	}

	@Override
	public No buscar(int chave) {
		if(inicio == null)
			System.out.println("Lista Vazia! ");
		else {
			No aux = inicio;
			while(aux.prox != null && aux.valor != chave) {
				aux = aux.prox;
			}
			if(aux.valor == chave) {
				return aux;
			}else {
				System.out.println("Valor n�o encontrado! ");
			}
		}
		
		return null;
	}

	@Override
	public void alterar(int chave, int novoValor) {

		if(inicio == null)
			System.out.println("Lista Vazia! ");
		else {
			No aux = inicio;
			while(aux.prox != null && aux.valor != chave) {
				aux = aux.prox;
			}
			if(aux.valor == chave) {
				aux.valor = novoValor;
				
			}else {
				System.out.println("Valor n�o encontrado! ");
			}
		}
		
	}

	@Override
	public void imprimir() {
		if(inicio == null) {
			System.out.println("Lista vazia! ");
		}else {
			No aux = inicio;
			
			while(aux != null) {
				System.out.println(aux.valor + " ");
				aux= aux.prox;
			}
		}
		
	}

}

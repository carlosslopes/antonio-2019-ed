package Lista4;

import java.util.Scanner;

public class questao02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe o valor: ");
		int n = sc.nextInt();
		Des2Bin(n);
	}

	private static void Des2Bin(int n) {
		if(n == 0) {
			return;
		}
		Des2Bin(n/2);
		System.out.print(n%2);
		
	}

}
